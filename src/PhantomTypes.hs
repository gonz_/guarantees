{-# LANGUAGE DataKinds, GADTs, MultiParamTypeClasses, PolyKinds,
  FlexibleInstances, ConstraintKinds #-}

module PhantomTypes where

import Tags (AddTag, DoesNotHaveTag, HasTag)

data User a = MkUser
  { _userName :: String
  , _userSensitiveInfo :: Maybe [String]
  } deriving (Show)

data LogSafe

data LogUnsafe

makeUserLogSafe :: User LogUnsafe -> User LogSafe
makeUserLogSafe (MkUser name _) = MkUser name Nothing

safeLog :: Show (a LogSafe) => a LogSafe -> IO ()
safeLog = print

user :: String -> [String] -> User LogUnsafe
user name sensitiveInfo = MkUser name (Just sensitiveInfo)

class MakeLogSafe datum where
  makeLogSafe :: datum LogUnsafe -> datum LogSafe

instance MakeLogSafe User where
  makeLogSafe = makeUserLogSafe

example :: IO ()
example = do
  let logUnsafeUser = user "Roger" ["Is rude"]
  let logSafeUser = makeLogSafe logUnsafeUser
  -- Will fail because it's already log-safe:
  -- "Couldn't match type ‘LogSafe’ with ‘LogUnsafe’"
  -- let logSafeUser' = makeLogSafe logSafeUser
  safeLog logSafeUser
  -- Will fail because it's not log-safe:
  -- "Couldn't match type ‘LogUnsafe’ with ‘LogSafe’"
  -- safeLog logUnsafeUser

-------------------------------------------------------------------------------
-- Turning it up to 11 for version 2 ------------------------------------------
-------------------------------------------------------------------------------
data Tag =
  LogSafeData
  deriving (Eq, Show)

class MakeLogSafeV2 datum where
  makeLogSafeV2 ::
       IsLogUnsafe datum tags => datum tags -> datum (AddTag 'LogSafeData tags)

safeLogV2 :: (Show (a tags), IsLogSafe a tags) => a tags -> IO ()
safeLogV2 = print

userV2 :: String -> [String] -> User '[]
userV2 name sensitiveInfo = MkUser name (Just sensitiveInfo)

instance MakeLogSafeV2 User where
  makeLogSafeV2 (MkUser name _) = MkUser name Nothing

type IsLogUnsafe d tags = DoesNotHaveTag 'LogSafeData tags ~ 'True

type IsLogSafe d tags = HasTag 'LogSafeData tags ~ 'True

exampleV2 :: IO ()
exampleV2 = do
  let logUnsafeUser = userV2 "Roger" ["Is rude"]
  let logSafeUser = makeLogSafeV2 logUnsafeUser
  -- Will fail because it's already log-safe: "Tags include: 'LogSafeData"
  -- let logSafeUser' = makeLogSafeV2 logSafeUser
  safeLogV2 logSafeUser
  -- Will fail because it's not log-safe: "Tags do not include: 'LogSafeData"
  -- safeLogV2 logUnsafeUser
