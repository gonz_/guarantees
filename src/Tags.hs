{-# LANGUAGE TypeOperators, DataKinds, GADTs, FlexibleInstances,
  TypeInType, FlexibleContexts, MultiParamTypeClasses, PolyKinds,
  TypeFamilies, UndecidableInstances #-}

module Tags where

import GHC.TypeLits (ErrorMessage(..), TypeError)

type family HasTag (t :: a) (ts :: [a]) :: Bool where
  HasTag tag '[] = TypeError ('Text "Tags do not include: " ':<>: 'ShowType tag)
  HasTag tag (tag : tags) = 'True
  HasTag tag (tag' : tags) = HasTag tag tags

type family DoesNotHaveTag (t :: a) (ts :: [a]) :: Bool where
  DoesNotHaveTag _ '[] = 'True
  DoesNotHaveTag tag (tag : _) = TypeError ('Text "Tags include: " ':<>: 'ShowType tag)
  DoesNotHaveTag tag (tag' : tags) = DoesNotHaveTag tag tags

type family AddTag (t :: a) (ts :: [a]) :: [a] where
  AddTag tag tags = (tag : tags)

type family RemoveTag (t :: a) (ts :: [a]) :: [a] where
  RemoveTag _ '[] = '[]
  RemoveTag tag (tag : tags) = RemoveTag tag tags
  RemoveTag tag (tag' : tags) = tag' : RemoveTag tag tags

type family ReplaceTag (ta :: a) (tb :: a) (ts :: [a]) :: [a] where
  ReplaceTag old new xs = (AddTag new (RemoveTag old xs))
