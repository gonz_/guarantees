guarantees
---------

* [PhantomTypes.hs](src/PhantomTypes.hs) has a demonstration of phantom types
  as well as usage of a type level tag system which allows a list of tags
  to be enforced instead of just one value per datum.
